build:
	@docker-compose -p jenkins build

run:
	@docker-compose -p jenkins up -d jenkins-slave

stop:
	@docker-compose -p jenkins stop

clean: stop
	@docker-compose -p jenkins rm jenkins-slave

clean-all: stop
	@docker-compose -p jenkins rm -v jenkins-slave

clean-images:
	@docker rmi $(docker images --quiet --filter "dangling=true")
